angular.module("socketStarter", [])
.factory("socket", function () {
  var socket = io()
  return socket
})
.controller("dataController", function ($scope, socket) {
  $scope.data = []
  socket.on("newData", function (datum) {
    $scope.data.push(datum)
    $scope.$digest()
  })
})
