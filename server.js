var app = require("./app")
var io = require("./socket")
var http = require("http")
require("./data")

var server = http.createServer(app)
io.attach(server)
server.listen(8080)
